// use this to test 'flatten'
const nestedArray = [1, [2], [[3]], [[[4]]]];
function flatten(arr) {
    let resultArr = []
    for (let index = 0; index < arr.length; index++) {
        if (arr[index] instanceof Array) {
            let res = flatten(arr[index]);
            resultArr = resultArr.concat(res);
        }
        else (resultArr.push(arr[index]));
    }
    return resultArr
}

module.exports = flatten;