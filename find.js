// Do NOT use .includes, to complete this function.
// * Look through each value in `elements` and pass each element to `cb`.
// * If `cb` returns `true` then return that element and stop.

const { moduleExpression } = require("@babel/types")

// * Return `undefined` if no elements pass the truth test.
function find(elements, cb) {
    for (let i = 0; i < elements.length; i++) {
        let checker = cb(elements[i])
        // console.log(checker)
        if (checker) return elements[i]
    }
    return undefined
}
// const items = [1, 2, 3, 4, 5, 5];
// function cb(i) {
//     return i > 4
// }
// let res = find(items, cb)
// console.log(res)
module.exports = find