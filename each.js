// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument
// based off http://underscorejs.org/#each
function each(elements, cb) {
    for (let index = 0; index < elements.length; index++) {
        cb(elements[index])
    }
    return undefined
}
// function cb(i) {
//     // console.log(i)
//     return
// }
// let a = [1, 2, 3, 4, 5, 5]
// let res = each(a, cb)
// console.log(res)

module.exports = each