// use this array to test your code. 
const items = [1, 2, 3, 4, 5, 5];
const { expect } = require("@jest/globals");
const customFilter = require("./filter")
function cb(i) {
    return i % 2 === 0;
}
const expectedResult = items.filter(cb)
// console.log(expectedResult)

let myResult = customFilter(items, cb)

test('should filter out only items that are correct according to callback', () => {
    expect(myResult).toEqual(expectedResult)
})