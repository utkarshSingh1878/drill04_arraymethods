const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

const customFlatten = require("./flatten")

const myRes = customFlatten(nestedArray)
const expectedRes = nestedArray.flat(Infinity)
// console.log(myRes)
// console.log(expectedRes)

test('should flatten the nested array', () => {
    expect(myRes).toEqual(expectedRes)
})