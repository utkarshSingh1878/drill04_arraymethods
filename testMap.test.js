// use this array to test your code. 
const items = [1, 2, 3, 4, 5, 5];
const { expect } = require("@jest/globals");
const customMap = require("./map")

function cb(i) {
    return i ** 2
}
let myRes = customMap(items, cb)
// console.log(myRes)

let expectedRes = items.map(cb)
// console.log(expectedRes)

test('should return newly genereted array by cb', () => {
    expect(myRes).toEqual(expectedRes)
})