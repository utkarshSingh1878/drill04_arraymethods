// use this array to test your code. 
const items = [1, 2, 3, 4, 5, 5];
const { expect } = require("@jest/globals");
const customEach = require("./each")

function cb(i) {
    console.log(i)
}
let res = customEach(items, cb)
let expectedOutput = undefined

test('should return undefined', () => {
    expect(res).toEqual(expectedOutput)
})