// use this array to test your code. 
const items = [1, 2, 3, 4, 5, 5];

function cb(startingValue, currentValue) {
    return startingValue * currentValue
}

const customReduce = require("./reduce")
const myRes = customReduce(items, cb, startingValue = null)
const expectedRes = items.reduce((prev, curr) => (prev * curr))
// console.log(myRes)
// console.log(expectedRes)

test('should return the reduced value genereted by cb/reducer', () => {
    expect(myRes).toBe(expectedRes)
})