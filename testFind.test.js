// use this array to test your code. 
const items = [1, 2, 3, 4, 5, 5];
const customFind = require("./find")
function cb(i) {
    return i > 40
}
let myRes = customFind(items, cb)
// console.log(myRes)

let expectedRes = items.find(cb)
// console.log(expectedRes)

test('should return single element allowed by cb or undefined', () => {
    expect(myRes).toEqual(expectedRes)
})