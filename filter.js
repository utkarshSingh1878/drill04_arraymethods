// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test
function filter(elements, cb) {
    let filteredData = []
    for (let index = 0; index < elements.length; index++) {
        let checker = cb(elements[index])
        if (checker) filteredData.push(elements[index]);
    }
    return filteredData
}
// function cb(i) {
//     return i % 2 === 0;
// }
// const items = [1, 2, 3, 4, 5, 5];
// const ans = filter(items, cb)
// console.log(ans)

module.exports = filter